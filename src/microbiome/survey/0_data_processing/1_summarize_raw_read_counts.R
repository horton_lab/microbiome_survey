## This R-script summarizes the read counts per sample (fasta.gz) file. 
## The code assumes that you have installed (gnu) parallel.
##
## Author: matt.horton
###############################################################################

rm(list=ls());

source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.miseq_microbiome_methods.R"));
#kingdom <- "bacteria_primer_comparison";
kingdom <- "bacteria";
omit <- "nonDmx|noDmx|Undetermined|uncut|revC|V567";

###############################################################################
## the two kingdom/data-types are stored in different folders.
###############################################################################
if( kingdom == "fungi" ){
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "/../raw/fungi/"));
	regexPattern <- "\\_R1\\_.*\\.fastq.gz$";

} else if( kingdom == "bacteria"){
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "/../raw/bacteria/"));
	regexPattern <- "\\_R1\\_.*\\.fastq.gz$";
	omit <- paste0( omit, "|primer_comparison");

} else if( kingdom == "bacteria_primer_comparison"){
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "/../raw/bacteria/primer_comparison"));
	regexPattern <- "\\_R1\\_.*\\.fastq.gz$";
}

gzFiles <- list.files(path=".", recursive=TRUE, pattern=regexPattern);
gzFiles <- gzFiles[grep(omit, invert=T, gzFiles)];

cat("There are:", length(gzFiles), "files to process; so use seq 0 ", length(gzFiles) - 1, "in your call, if you're using parallel.\n");
writeJobToExtractReadCountsFromFastqGZfiles(gzFiles);

###############################################################################
## Steps:
## 1.) fire the above file (local_determine_raw_readcounts_n.sh). Using parallel, the bash code might be of the form:
## bash_env$ seq 0 <n> | parallel -j <ncpus> ./local_determine_raw_readcounts_<n>.sh {}
##
## note that '<n>' should be changed to your number of files, and <ncpus> should be replaced with the number of cpus you want to use.
##
## 2.) to combine all of the *_read_counts.txt files, you could use the following bash command (tested in OSX):
## bash_env$ find . -name "*gz_read_counts.txt" | xargs -I '{' grep -H "" '{' | sed -e 's/:/     /;s/\.\///' > fastq_read_counts.txt
## nb: if you copy the above command, the white space in the second half of the sed argument will result in spaces (not a tab)
##
## this finds all of the read_count files, iterates over them, attaching the filename, replacing the colon with a tab, and getting rid of the relative filepath name...
## avoid using an output filename that matches the find -name pattern.
## 
## Results:
## Comparing V34 and V567 in two separate runs results in:
## nb: this is leaf bacteria (endos) for European samples.
## run1: 11700678
## run2: 14168547
## read counts, which we estimated using the bash command:
## bash_env$ cut -f 2 fastq_read_counts.txt | paste -sd+ - | bc
##
##
## ITS/read counts (overall)
## all runs: 71888674
## run1: 17286823
## run2: 6123582
## run3: 17053184
## run4: 31425085

## and these reads characterize the following # of samples:
# bot0241m:fungi mhorto$ cut -f 1 -d "/" fastq_read_counts.txt  | sort | uniq -c
#
#
#
#

## V34/V567, primer_comparison (overall)
## all runs: 25869225
## run1: 11700678
##    * V34: 6646078
##    * V567: 5054600
## run2: 14168547
##    * V34: 3587338
##    * V567: 5443136
##    * Undetermined: 5138073
