# TODO: Add comment
# 
# Author: mhorto
###############################################################################

rm(list=ls());

###############################################################################
## user variables; marker
###############################################################################
sequencingRegion <- "ITS";
otuThreshold <- 0.97;

###############################################################################
clusterOTUsFromDada <- function( hypervariableRegion, otuCutoff, metadataFile, classifier, numberOfThreads=2 ){

	cat("Writing the qiime-shell script now.\n\n");
	filename <- paste0(getwd(), "/local_qiime_cluster_otus_", otuCutoff, ".sh");

	scriptString <- paste0(
			"#!/bin/bash\n",
			"region='", hypervariableRegion, "'\n",
			"log_file='", gsub(".sh", ".log", filename), "'\n",
			"otu_cutoff=", otuCutoff, "\n",
			"metadata_file='", metadataFile, "'\n",
			"cd ", getwd(), "/runs_merged/dada_", hypervariableRegion, "/\n\n");

	###############################################################################
	## OTU clustering with the feature tables from DADA
	###############################################################################
	scriptString <- paste0( scriptString,
			"qiime vsearch cluster-features-de-novo \\\n",
					" --i-sequences ${region}-rep-seqs.qza \\\n",
					" --i-table ${region}-feature-table.qza \\\n",
					" --p-perc-identity ${otu_cutoff} \\\n",
					" --o-clustered-table ${region}-otu_${otu_cutoff}-feature-table.qza \\\n",
					" --p-threads ", numberOfThreads, " --verbose \\\n",
					" --o-clustered-sequences ${region}-otu_${otu_cutoff}-rep-seqs.qza >> ${log_file}\n",
			"\n\n",  
			"echo \">>>>>>The sequences and features were clustered at ", (otuCutoff * 100), "% identity with vsearch<<<<<<\" >> ${log_file}\n\n");

	###############################################################################
	## write out the clustered (OTU) feature table
	###############################################################################
	scriptString <- paste0( scriptString, 
			"qiime tools export --input-path ${region}-otu_${otu_cutoff}-feature-table.qza --output-path table-export >> ${log_file}\n",
			"mv table-export/feature-table.biom table-export/${region}-otu_${otu_cutoff}-feature-table.biom\n",
			"biom convert -i table-export/${region}-otu_${otu_cutoff}-feature-table.biom -o table-export/${region}-otu_${otu_cutoff}-feature-table.txt --to-tsv >> ${log_file}\n" );

	###############################################################################
	## create the rep-seqs text file from the rep-seqs.qza file
	###############################################################################
	scriptString <- paste0( scriptString, 
					"\n",
					"qiime tools export --input-path ${region}-otu_${otu_cutoff}-rep-seqs.qza --output-path .\n");
	scriptString <- paste0( scriptString, 
					"mv dna-sequences.fasta ${region}-otu_${otu_cutoff}-rep-seqs.txt\n\n");

	###############################################################################
	## assign status to the OUTPUT file ; use requested classifier
	###############################################################################
	scriptString <- paste0( scriptString, "\ncd taxonomy\n");

	scriptString <- paste0( scriptString,
			"qiime feature-classifier classify-sklearn \\\n",
					"  --i-classifier ", classifier, " \\\n",
					"  --i-reads ../${region}-otu_${otu_cutoff}-rep-seqs.qza \\\n",
					"  --o-classification ${region}-otu_${otu_cutoff}-taxonomy.qza >> ${log_file}\n",

			"qiime taxa barplot \\\n",
					" --i-table ../${region}-otu_${otu_cutoff}-feature-table.qza \\\n",
					" --i-taxonomy ${region}-otu_${otu_cutoff}-taxonomy.qza \\\n",
					" --m-metadata-file ${metadata_file} --o-visualization ${region}-otu_${otu_cutoff}-taxa-barplot.qzv >> ${log_file}\n",

			"\nqiime tools export --input-path ${region}-otu_${otu_cutoff}-taxonomy.qza --output-path taxonomy-export >> ${log_file}\n",
			"mv taxonomy-export/taxonomy.tsv taxonomy-export/${region}-otu_${otu_cutoff}-taxonomy.tsv\n",
			"echo \">>>>>>The taxonomy data have been extracted/written.<<<<<<\" >> ${log_file}\n");

	scriptString <-  paste0( scriptString, "echo \"The qiime-script file is complete. Enjoy your day.\"\n");

	sink(filename);
	cat(scriptString);
	sink(NULL);
}

###############################################################################
## here, we iterate over the user's selected directory/directories
## and find the requested feature tables/sequencing tables
###############################################################################
if( sequencingRegion == "16S" ){ ## 
	regionalMetadataFile <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "SampleMetadata.txt");
	taxonomicClassifier <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "bacteria/classifiers/classifier_silva97.qza");
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "bacteria/"));

} else if( substr(sequencingRegion, 0, 1) == "V" ){ ## the primer comparison project...
	regionalMetadataFile <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "bacteria/BothHypervariableRegions_MetadataPrimerComp.txt");
	taxonomicClassifier <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "bacteria/classifiers/classifier_silva97.qza");
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "bacteria/primer_comparison/"));
	
} else if( sequencingRegion == "ITS" ){
	regionalMetadataFile <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "SampleMetadata.txt");
	taxonomicClassifier <- paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "fungi/UNITE/2019fungi/classifier_UNITE_dyn.qza"); ##
	setwd(paste0(Sys.getenv('VESCA_MICROBIOME_DATA'), "fungi/"));
}

if(( file.exists(regionalMetadataFile)) & ( file.exists(taxonomicClassifier))){
	###############################################################################
	## here, we write the shell script responsible for clustering OTUs
	## and assigning taxonomic status to the final file
	###############################################################################	
	clusterOTUsFromDada(
			hypervariableRegion=sequencingRegion,
			otuCutoff=otuThreshold,
			metadataFile=regionalMetadataFile,
			classifier=taxonomicClassifier);
}
