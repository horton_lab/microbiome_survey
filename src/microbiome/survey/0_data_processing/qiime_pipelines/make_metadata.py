#!/usr/bin/env python3
import glob
from os import chdir, getcwd
import pandas as pd
import sys


def build_sample_file(args):
    if args[1] is None:
        cwd = getcwd() + '/'
    else:
        cwd = args[1] + '/'
        chdir(cwd)

    gz = glob.glob("*R1*gz")
    sample_ids = [gz_i.upper().split("_R1")[0].split(".")[-1] for gz_i in gz]
    markers = [gz_i.split("_")[-1] for gz_i in sample_ids]
    kingdoms = ["Bacteria" if ("16S" in marker_i) else "Fungi" for marker_i in markers]
    sample_info = pd.DataFrame({'sample_id': sample_ids, 'kingdom': kingdoms, 'region': markers})

    output_fname = cwd + 'SampleMetadata.txt'
    sample_info.to_csv(output_fname, sep='\t', header=True, index=False)

def main(args):
    try:
        build_sample_file(args)
        print("done", flush=True)
        return 0
    except KeyboardInterrupt:
        # handle keyboard interrupt ###
        return 0
    except Exception as e:
        # todo: eventually add logging
        print(e, flush=True)
        return 2


if __name__ == "__main__":
    sys.exit(main(sys.argv))
