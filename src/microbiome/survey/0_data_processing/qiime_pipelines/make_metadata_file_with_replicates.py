#!/usr/bin/env python3
import glob
import os
import sys


def build_sample_file_agribiome(args):
    if args[1] is None:
        cwd = os.getcwd() + '/'
    else:
        cwd = args[1] + '/'
        os.chdir(cwd)

    gz = glob.glob("*fastq.gz")
    if len(gz) == 0:
        sys.exit(2)

    # rename the files...
    for gz_i in gz:
        new_name = gz_i.replace("_bac_", "_16S_").replace("Scre17_", "Scre17.", 1)
        if (not os.path.isfile(gz_i)):
            print(f"The file: {gz_i} was not found.")
            continue

        # it has already been renamed
        if new_name == gz_i: continue
        print(f"Will replace: {gz_i} with the new name: {new_name}")
        try:
            os.rename(gz_i, new_name)
        except Exception as e:
            print(e, flush=True)
            sys.exit(2)

    try:
        gz_r1 = glob.glob("*R1*fastq.gz")
        sample_info = []
        for elem_i, r1_i in enumerate(gz_r1):
            slices = r1_i.split("_R1.fastq.gz")[0].upper().split(".")[-1].split("_")
            if(len(slices) == 3): # irregular partitions rule out list comprehensions with zip
                sample_info.append(slices)
            else:
                print(f"Dropping element: {elem_i}, or {'_'.join(slices)}, size: {len(slices)}\n")
                continue

        sample_df = pd.DataFrame(sample_info, columns=['SampleID', 'replicate_id', 'marker'])
        sample_df['kingdom'] = ['Bacteria' if( marker_i == "16S" ) else "Fungi" if(marker_i == "ITS") else "Unknown" for marker_i in sample_df.marker]

        # write the file out
        output_fname = cwd + '/SampleMetadata.txt'
        sample_df.to_csv(output_fname, sep='\t', header=True, index=False)

    except KeyboardInterrupt:
        # handle keyboard interrupt ###
        return 0
    except Exception as e:
        # todo: eventually add logging
        print(e, flush=True)
        return 2


if __name__ == "__main__":
    sys.exit(build_sample_file_agribiome(sys.argv))
