#!/usr/bin/env python3
import glob
from os import chdir, getcwd
import pandas as pd
import re
import sys


def build_manifest(args):
    if args[1] is None:
        cwd = getcwd() + '/'
    else:
        cwd = args[1] + '/'
        chdir(cwd)

    gz = glob.glob("*gz")

    # find the sample_id for either R1 or R2
    sample_ids = [re.split("_R[12]", gz_i.upper())[0].split(".")[-1] for gz_i in gz]
    filepaths = [cwd + gz_i for gz_i in gz]
    direction = ["forward" if ("_R1." in gz_i) else "reverse" if "_R2." in gz_i else "Unknown" for gz_i in gz]

    manifest = pd.DataFrame({'sample-id': sample_ids, 'absolute-filepath': filepaths, 'direction': direction})
    manifest.sort_values(['sample-id', 'direction'], inplace=True)
    output_fname = cwd + 'ManifestRun.csv'
    manifest.to_csv(output_fname, sep=',', header=True, index=False)


def main(args):
    try:
        print(sys.argv)
        build_manifest(args)
        print("done", flush=True)
        return 0
    except KeyboardInterrupt:
        # handle keyboard interrupt ###
        return 0
    except Exception as e:
        # todo: eventually add logging
        print(e, flush=True)
        return 2


if __name__ == "__main__":
    sys.exit(main(sys.argv))
