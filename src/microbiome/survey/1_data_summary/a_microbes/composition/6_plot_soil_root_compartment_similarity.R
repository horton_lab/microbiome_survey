## This R-script uses ggplot/facets to visualize differences among
## the soil, rhizosphere, and root endophytic compartments.
## 
## Author: matt.horton
###############################################################################

rm(list=ls());

library(ggplot2);

source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.miseq_microbiome_methods.R"));
source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.base_methods.R"));

###############################################################################
## parameters
###############################################################################
# targets:
otuThreshold <- 1;
minimumNumberOfReads <- 1;
sequencingRegion <- "16S";
dadaVariables <- c(TRUE, 0.97); ## either a simple boolean (whether or not to use dada) or a 2-item vector; in the latter case, an otu pairwise similarity threshold can be indicated
logYAxis <- FALSE;

## possibilities: from phylum to genus...
taxonomicLevels <- c("phylum");

if( sequencingRegion == "16S" ){
	taxaToHighlight <- list(phylum=c("Proteobacteria", "Actinobacteria", "Acidobacteria"));
	
} else {
	taxaToHighlight <- list(phylum=c("Ascomycota", "Basidiomycota"));
}

###############################################################################

{
	###############################################################################
	## retrieve the dataset
	###############################################################################
	dataset.robj <- getData(tissue="all", region=sequencingRegion, minReadsPerSite=minimumNumberOfReads, excludeHostDNA=TRUE, dada=dadaVariables, rarefy=(minimumNumberOfReads > 1));
	dataset.cov <- subset( dataset.robj$cov, host_name != "Arabidopsis thaliana" );
	dataset.data <- dataset.robj$data;
	dataset.taxa <- dataset.robj$taxa;

	###############################################################################
	## create the output directory if necessary
	###############################################################################
	outputDirectory <- paste0( Sys.getenv('VESCA_OUTPUT_DIR'), "images/data_summary/microbes/", sequencingRegion, "/" );
	stopifnot(dir.exists(outputDirectory));

	datasets <- list();
	for( transect_l in c("US", "Europe")){

		###############################################################################
		## subset to the geographic region of interest
		###############################################################################
		selectTransectLogicalOperator <- ifelse(transect_l == "US", "country == \"US\"", "country != \"US\"");
		regional.cov <- subset(dataset.cov, eval(parse(text=selectTransectLogicalOperator)));		
		cat("Subsetting to transect:", transect_l, "\n");

		###############################################################################
		## subset to the bulk/unplanted soil
		###############################################################################
		subset.cov <- subset( regional.cov, organ == "soil" | organ == "R" );
		subset.data <- dataset.data[,subset.cov[,"SampleID"]];

		###############################################################################
		## get the offsets
		###############################################################################
		subset.offsets <- mstack(colSums(subset.data), sorted=FALSE, newHeaders=c("sample_id", "offset"));
		subset.robj <- applyThreshold( subset.data, otuThreshold, dataset.robj$taxa[rownames(subset.data),] );
		subset.data <- subset.robj$data;
		subset.taxa <- subset.robj$taxa;

		###############################################################################
		## subset to the soil
		###############################################################################
		soil.cov <- subset(subset.cov, organ == "soil" );
		soil.data <- subset.data[,soil.cov[,"SampleID"]];
		soil.data <- soil.data[which(rowSums(soil.data) > 0),];
		cat("\n****In the ", transect_l, " soil community, there are: ", nrow(soil.data), " ", sequencingRegion, " asvs/OTUS using a threshold of: ", otuThreshold, ".****\n\n", sep="");
		
		###############################################################################
		## subset to the root epiphytes
		###############################################################################
		root_epis.cov <- subset(subset.cov, lifestyle == "epiphyte" );
		root_epis.data <- subset.data[,root_epis.cov[,"SampleID"]];
		root_epis.data <- root_epis.data[which(rowSums(root_epis.data) > 0),];
		cat("\n****In the ", transect_l, " root epiphytic community, there are: ", nrow(root_epis.data), " ", sequencingRegion, " asvs/OTUS using a threshold of: ", otuThreshold, ".****\n\n", sep="");
		
		###############################################################################
		## subset to the root epiphytes
		###############################################################################
		root_endos.cov <- subset(subset.cov, lifestyle == "endophyte" );
		root_endos.data <- subset.data[,root_endos.cov[,"SampleID"]];
		root_endos.data <- root_endos.data[which(rowSums(root_endos.data) > 0),];
		cat("\n****In the ", transect_l, " root endophytic community, there are: ", nrow(root_endos.data), " ", sequencingRegion, " asvs/OTUS using a threshold of: ", otuThreshold, ".****\n\n", sep="");
		
		abundances <- list();
		for( j in 1:length(taxonomicLevels)){

			level_j <- taxonomicLevels[j];
			taxaToHighlight_j <- taxaToHighlight[[level_j]];

			for( k in 1:length(taxaToHighlight_j)){
				taxon_k <- taxaToHighlight_j[k];
				otuIds <- subset.taxa[which( subset.taxa[,level_j] == taxon_k ), "otu_id"];

				indices <- which(otuIds %in% rownames(soil.data));
				soil.k <- data.frame( transect=ifelse( transect_l == "US", "North America", transect_l), 
									taxon=taxon_k, mstack(colSums(soil.data[otuIds[indices],]), sorted=FALSE, 
									newHeaders=c("sample_id", level_j )), type="Soil", stringsAsFactors=FALSE );
				soil.k <- merge( soil.k, subset.offsets, by="sample_id" );
				
				indices <- which(otuIds %in% rownames(root_epis.data));
				epis.k <- data.frame( transect=ifelse( transect_l == "US", "North America", transect_l), 
									taxon=taxon_k, mstack(colSums(root_epis.data[otuIds[indices],]), sorted=FALSE, 
									newHeaders=c("sample_id", level_j )), type="Rhizosphere", stringsAsFactors=FALSE );
				epis.k <- merge( epis.k, subset.offsets, by="sample_id" );
				
				indices <- which(otuIds %in% rownames(root_endos.data));
				endos.k <- data.frame( transect=ifelse( transect_l == "US", "North America", transect_l), 
									taxon=taxon_k, mstack(colSums(root_endos.data[otuIds[indices],]), sorted=FALSE, 
									newHeaders=c("sample_id", level_j )), type="Root endophytes", stringsAsFactors=FALSE );

				endos.k <- merge( endos.k, subset.offsets, by="sample_id" );
				total.k <- rbind(soil.k, epis.k, endos.k);
				total.k <- within(total.k, {
							proportion <- phylum / offset;
						});

				abundances[[level_j]][[taxon_k]] <- total.k; ## ); rbind(abundances[[level_j]], 
			}
		}

		datasets[[transect_l]] <- abundances;
	}

	for( j in 1:length(taxonomicLevels)){

		both <- list();
		for( transect_l in c("US", "Europe")){	
			both[[transect_l]] <- do.call( rbind, datasets[[transect_l]][taxonomicLevels[j]][[1]] );
		}
		
		both <- do.call(rbind, both);
		both <- within(both, {
					habitat <- factor(type, levels=c("Soil", "Rhizosphere", "Root endophytes"));
					transect <- factor(transect, levels=c("North America", "Europe"));
					proportion <- proportion + 0.0001
				});

		###############################################################################
		## plot the overall phylum distribution per transect
		###############################################################################
		gg <- ggplot( both, aes(x=transect, y=proportion, colour=taxon)) + 
				theme_classic(base_size=20) + 
				theme(
						axis.text.x=element_text(size=16, hjust=1, angle=25), 
						axis.text.y=element_text(size=16),
						legend.key.size=unit(1.3, "line"),
						legend.background=element_blank(),
						legend.position = c(0.01, 0.99),
						legend.justification=c("left", "top"),
						panel.background = element_rect(fill = NA, color = "black"),
						rect=element_rect(fill="transparent"),
						strip.background = element_blank(),
						strip.text.x=element_text(size=16)) +
				facet_grid(. ~ habitat) + 
				geom_boxplot(outlier.size=-1) + 
				geom_jitter(position=position_jitterdodge(0.1), size=2) + 
				ylab( "Proportion" ) + 
				xlab( "Microbial habitat" ) + labs(colour="Taxon");

		if( logYAxis ){
			gg <- gg + scale_y_continuous(trans='log10', limits=c(0.005,1)) + 
					theme( legend.position=c(0.01, 0.01),
							legend.justification=c("left", "top"));
		}

		outputFileName <- paste0(outputDirectory, sequencingRegion, "", taxonomicLevels[j], ".soil_roots", ifelse(logYAxis, "_log", ""), ".pdf" );
		ggsave(gg, file=outputFileName, device="pdf", scale=1, width=10, height=10, useDingbats=FALSE);
	}
}
