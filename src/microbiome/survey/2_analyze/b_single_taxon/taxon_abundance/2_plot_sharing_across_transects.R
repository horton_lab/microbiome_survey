## This R-script plots the relationship b/w individual ASVs/OTUs and 
## climate/soil variables (and any possible interactions)
## 
## Author: matt.horton
###############################################################################

rm(list=ls());

source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.miseq_microbiome_methods.R"));
source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.base_methods.R"));

###############################################################################
## variables:
###############################################################################
minimumNumberOfReads <- 5000;
otuThreshold <- 2;

dadaVariables <- c(TRUE, 0.97); ## either a simple boolean (whether or not to use dada) or a 2-item vector; in the latter case, an otu pairwise similarity threshold can be indicated
sequencingRegion <- "ITS";
numberOfTaxaToPlot <- 250;

sampleColors <- list(L_epiphyte="cadetblue3", L_endophyte="chartreuse3", R_epiphyte="burlywood", R_endophyte="burlywood4", soil_="ivory4");
sampleSymbols <- list(L_epiphyte=24, L_endophyte=25, R_epiphyte=21, R_endophyte=23, soil_=22);

privateOTUColor <- "coral1"; ## set to NA to use the same color for shared and private (have also used 'tan1', 'orchid', etc. in the past)
separatorLine <- TRUE;

###############################################################################

{
	###############################################################################
	## retrieve the overall dataset for this sequencing region
	###############################################################################
	dataset.robj <- getData(tissue="all", region=sequencingRegion, minReadsPerSite=minimumNumberOfReads, excludeHostDNA=TRUE, dada=dadaVariables, rarefy=(minimumNumberOfReads > 1));
	dataset.cov <- dataset.robj$cov;
	dataset.cov <- dataset.cov[,(!names(dataset.cov) %in% c("latitude", "longitude"))];
	dataset.data <- dataset.robj$data;
	dataset.taxa <- dataset.robj$taxa;
	
	###############################################################################
	## output directory
	###############################################################################
	outputDirectory <- paste0( Sys.getenv('VESCA_OUTPUT_DIR'), "images/single_otus/transects/",  sequencingRegion, "/" );
	if( !dir.exists(outputDirectory)){
		dir.create(outputDirectory, recursive=TRUE);
		cat("Created output directory:", outputDirectory, "\n");
	}
	
	for( organ_j in c( "L", "R" )){
		
		for( habitat_k in c("epiphyte", "endophyte")){

			organAndHabitat <- paste0(organ_j, "_", habitat_k);
			datasets <- list(); 

			for( transect_l in c("US", "Europe")){
				
				###############################################################################
				## subset to the geographic region of interest
				###############################################################################
				if( transect_l != "all" ){
					cat("Subsetting to transect:", transect_l, "\n");
					selectTransectLogicalOperator <- ifelse(transect_l == "US", "country == \"US\"", "country != \"US\"");
					regional.cov <- subset(dataset.cov, eval(parse(text=selectTransectLogicalOperator)));
					
				} else {
					regional.cov <- dataset.cov; ## set to the entire/worldwide geographic region.
				}
				
				cat("###############################################################################\n");
				cat("Organ: ", organ_j, "\n");
				cat("###############################################################################\n");
				cat("----Habitat: ", habitat_k, ".\n", sep="");
				cat("--------Transect: ", transect_l, "\n");
				cat("###############################################################################\n");
				
				###############################################################################
				## subset to the requested organ/habitat
				###############################################################################
				subset.cov <- subset(regional.cov, organ == organ_j & lifestyle == habitat_k);
				subset.data <- dataset.data[,subset.cov[,"SampleID"]];
				subset.data <- subset.data[which(rowSums(subset.data) > 0),];
				subset.data <- normalizeInSomeWay( subset.data, normalizationMethod="aldex2", eliminateSingletons=(otuThreshold > 1));
				cat("\n****In the ", transect_l, " ", organ_j, " ", habitat_k, " community, there are: ", length(which(rowSums(subset.data) > 0)), " ", sequencingRegion, " asvs/OTUS.****\n\n", sep="");

				###############################################################################
				## 
				#############################################################################
				subset.data <- t(subset.data[order(apply(subset.data, 1, median), decreasing=T)[1:numberOfTaxaToPlot],]);
				summaries <- data.frame( rank=numeric(), otu_id=character(), taxon=character(), min=numeric(), lower_quantile=numeric(), 
								median=numeric(), upper_quantile=numeric(), max=numeric(), stringsAsFactors=FALSE );

				for( otu_m in 1:numberOfTaxaToPlot ){
					summaries[nrow(summaries) + 1, "rank"] <- otu_m;
					summaries[nrow(summaries), "otu_id"] <- otuId <- colnames(subset.data)[otu_m];
					summaries[nrow(summaries), "taxon"] <- getLowestTaxonomicAssignment( dataset.taxa[otuId,] );
					summaries[nrow(summaries), "min"] <- min(subset.data[,otu_m]);
					summaries[nrow(summaries), "lower_quantile"] <- quantile(subset.data[,otu_m], 0.25);
					summaries[nrow(summaries), "median"] <- median(subset.data[,otu_m]);
					summaries[nrow(summaries), "upper_quantile"] <- quantile(subset.data[,otu_m], 0.75);
					summaries[nrow(summaries), "max"] <- max(subset.data[,otu_m]);
				}

				datasets[[paste0(transect_l, "_summary")]] <- summaries;
			}

			usMatrix <- datasets[["US_summary"]];
			euMatrix <- datasets[["Europe_summary"]];

			pdfOutputFileName <- paste0(outputDirectory, "US_Europe_overlap.", sequencingRegion, ".n", numberOfTaxaToPlot,
                                        "", organAndHabitat, ".", minimumNumberOfReads,
                                        ifelse( length(dadaVariables) == 2, paste0(".otus", dadaVariables[2]), ""),
                                        ifelse( is.na(privateOTUColor), "", paste0( ".", privateOTUColor)),
                                        ".tn", otuThreshold,
                                        ifelse( separatorLine, ".line", "" ),
                                        ".pdf");
			
			pdf( pdfOutputFileName, width=8, height=8 );
			par(bty="L"); ## use bottom and left box boundaries
			compareRanks( numberOfTaxaToPlot,
					lhsMatrix=usMatrix, 
					rhsMatrix=euMatrix, 
					abundanceColumnName="median",
					lhsLabel=paste0( "North America" ), 
					rhsLabel=paste0( "Europe" ),
					yLabel=paste0( "CLR, ", ifelse( sequencingRegion == "ITS", "fungi", "bacteria" )),
					alpha=25,
					pchSize=2,
					sharedPch=unlist(sampleSymbols[organAndHabitat]),
					sharedColor=unlist(sampleColors[organAndHabitat]), 
					sharedFill=unlist(sampleColors[organAndHabitat]),
					overlapPrivateTaxa=FALSE,
					privatePch=22,
					privateColor=ifelse( is.na( privateOTUColor ), unlist(sampleColors[organAndHabitat]), privateOTUColor ),
					privateFill=ifelse( is.na( privateOTUColor ), unlist(sampleColors[organAndHabitat]), privateOTUColor ),
					privateLegendLabel="Unique");

			if( separatorLine ){
				abline( v=0.5, lty=2, col=translucify("grey"), lwd=3);
			}

			dev.off();
		}
	}
}



