## This R-script performs PCA/RDA on a single kingdom, for a given organ,
## lifestyle (epi/endo), and transect. Then, vegan::ordiR2step is used
## to identify the climate and/or soil variables that are influential
## 
## Author: matt.horton
###############################################################################

rm(list=ls());

library(vegan); library(adespatial);

source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.miseq_microbiome_methods.R"));
source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.base_methods.R"));

###############################################################################
## metagenomic variables:
###############################################################################
minimumNumberOfReads <- 1;
otuThreshold <- 2;
dadaVariables <- c(TRUE, 0.97); ## either a simple boolean (whether or not to use dada) or a 2-item vector; in the latter case, an otu pairwise similarity threshold can be indicated
sequencingRegion <- "16S";

numberOfPermutations <- 99999;

###############################################################################
## pca variables:
###############################################################################
dataNormalizationMethod <- "aldex2";
numbersOfTaxaToTest <- c(250); ## also possible: "all"

bioclim <- FALSE;
focalSeasons <- c("spring", "summer");

###############################################################################

{
	###############################################################################
	## retrieve the dataset
	###############################################################################
	dataset.robj <- getData(tissue="all", region=sequencingRegion, minReadsPerSite=minimumNumberOfReads, excludeHostDNA=TRUE, dada=dadaVariables, rarefy=(minimumNumberOfReads > 1));
	dataset.cov <- subset( dataset.robj$cov, host_name != "Arabidopsis thaliana" );
	dataset.cov <- dataset.cov[,which(!colnames(dataset.cov) %in% c("latitude", "longitude"))];
	dataset.data <- dataset.robj$data;
	dataset.taxa <- dataset.robj$taxa;
	
	###############################################################################
	## get the climate data!
	###############################################################################
	environmentalData <- getEnvironmentalData( "climate_and_soil", seasons=focalSeasons, bioclimSettings=bioclim );
	environmentalData <- subset(environmentalData, select = -c(aspect, sin_aspect, cos_aspect));
	environmentalData[,"aridity"] <- -1 * environmentalData[,"aridity"]; ## invert for intuitiveness; without this transformation high values are assigned to wet sites yet the variable name is ARIDITY 

	environmentalVariables <- c("elevation", colnames(environmentalData)[-c(1:2)]);
	dataset.cov <- merge(dataset.cov, environmentalData, by=c("plant_id", "site"));

	for( organ_j in c( "L", "R" )){

		for( habitat_k in c("epiphyte", "endophyte")){

			for( transect_l in c("US", "Europe")){

				###############################################################################
				## subset to the geographic region of interest
				###############################################################################
				if( transect_l != "all" ){
					cat("Subsetting to transect:", transect_l, "\n");
					selectTransectLogicalOperator <- ifelse(transect_l == "US", "country == \"US\"", "country != \"US\"");
					regional.cov <- subset(dataset.cov, eval(parse(text=selectTransectLogicalOperator)));
					
				} else {
					regional.cov <- dataset.cov; ## set to the entire/worldwide geographic region.
				}
				
				cat("###############################################################################\n");
				cat("Organ: ", organ_j, "\n");
				cat("###############################################################################\n");
				cat("----Habitat: ", habitat_k, ".\n", sep="");
				cat("--------Transect: ", transect_l, "\n");
				cat("###############################################################################\n");

				###############################################################################
				## subset to the requested organ/habitat
				################# ##############################################################
				subset.cov <- subset(regional.cov, organ == organ_j & lifestyle == habitat_k);
				subset.data <- dataset.data[,subset.cov[,"SampleID"]];
				subset.data <- subset.data[which(rowSums(subset.data) > 0),];

				cat("Normalization method:", dataNormalizationMethod, "\n");
				subset.data <- normalizeInSomeWay( subset.data, 
													normalizationMethod=ifelse( is.na(dataNormalizationMethod), "raw", dataNormalizationMethod), 
													eliminateSingletons=(otuThreshold > 1));

				subset.data <- subset.data[order(rowSums(subset.data), decreasing=T),];
				cat("\n****In the ", transect_l, " ", organ_j, " ", habitat_k, " community, there are: ", nrow(subset.data), " ", sequencingRegion, " asvs/OTUS using a threshold of: ", otuThreshold, ".****\n\n", sep="");

				###############################################################################
				## subsetting the data might save time...
				###############################################################################
				if( !"all" %in% numbersOfTaxaToTest ){
					cat("In the interests of time (namely, to speed up pca and envfit), we are subsetting the data.\n");
					subset.data <- subset.data[1:(min(max(numbersOfTaxaToTest), nrow(subset.data))),];
				}

				## aldex is memory intensive
				gc();

				###############################################################################
				## use stepwise regression, with RDA, to identify the key environmental variables
				###############################################################################
				for( m in 1:length( numbersOfTaxaToTest )){
					nspecies_m <- numbersOfTaxaToTest[m];
					cat("Working with:", nspecies_m, "species.\n");

					###############################################################################
					## subset and align the covariates accordingly before renaming the colnames
					###############################################################################
					plant.subset_m <- t(subset.data[1:nspecies_m,]);
					env.cov <- subset.cov[match(rownames(plant.subset_m), subset.cov[,"SampleID"]),];
					rownames(plant.subset_m) <- env.cov[, "plant_id"];
					rownames(env.cov) <- env.cov[, "plant_id"];
					env.cov <- na.omit(env.cov[,environmentalVariables]);
					plant.subset_m <- plant.subset_m[rownames(env.cov),];
					
					###############################################################################
					## perform the rda and use forward selection to identify the relevant variables
					## see: https://www.davidzeleny.net/anadat-r/doku.php/en:forward_sel_examples
					###############################################################################				
					rda_m0.robj <- rda( plant.subset_m ~ Condition(latitude + longitude), data=env.cov, scale=TRUE );
					rda_m1.robj <- rda( plant.subset_m ~ Condition(latitude + longitude) + ., data=env.cov, scale=TRUE );
					
					forwardSelection <- anovaTable <- ordiR2step( rda_m0.robj, scope=formula(rda_m1.robj), R2scope=FALSE, permutations=numberOfPermutations )$anova; 
					
					###############################################################################
					## refit the model with the selected variables to estimate r-squared
					###############################################################################				
					variables <- gsub("^\\+ ", "", rownames(forwardSelection));
					rda_m2.robj <- rda( plant.subset_m ~ Condition(latitude + longitude) + ., data=env.cov[,c("latitude", "longitude", variables)]);
					
					forwardSelection <- data.frame( variables=variables, organ=organ_j, habitat=habitat_k, transect=transect_l, rsquare=RsquareAdj(rda_m2.robj)$r.squared, forwardSelection, stringsAsFactors=FALSE );
					colnames(forwardSelection)[ncol(forwardSelection)] <- "pvalue";
					forwardSelection$pval.adj <- p.adjust(forwardSelection$pvalue, method='fdr', n=length(environmentalVariables));
					
					###############################################################################
					## identify (and if necessary create) the output directory
					###############################################################################
					outputDirectory <- paste0( Sys.getenv('VESCA_OUTPUT_DIR'), "tables/diversity/beta/rda/env_", transect_l, "/", sequencingRegion, "/p", 1/(1 + numberOfPermutations), "/" ); ##
					
					if( !dir.exists(outputDirectory)){
						dir.create(outputDirectory, recursive=TRUE);
						cat("Created output directory:", outputDirectory, "\n");
					}
					
					print( forwardSelection );
					robjectsOutputFileName <- paste0(outputDirectory, "rda.", organ_j, "_", habitat_k, "_", transect_l, "", minimumNumberOfReads, ".", sequencingRegion, ".", nspecies_m, ".", dataNormalizationMethod, ".p", (1/(1+numberOfPermutations)), ".regress_geography.robj" );
					save( rda_m1.robj, forwardSelection, file=robjectsOutputFileName );
					gc();
				}
			}
		}
	}
}
