### Analyzing the Fragaria microbiome

Authors: Matthew W. Horton (horton.matthew.w@gmail.com), 

This package contains code (R-scripts) for analyzing plant-microbial communities. The code was developed for *Fragaria vesca*, but can be applied to other organisms provided that the data are in the ASV/OTU-table file format of Qiime. Suggestions and code contributions are welcome.

The current version is 1.0

### The main dependencies are:
* R (3.4.3+)
* The R-package vegan
* qiime
* ...

### Setting the environment

The R scripts use _Sys.getenv()_ to avoid hardcoded paths.  
You will need to modify the variables within .Renviron (main folder) to fit your environment.  

*conda users: the yaml file and environmental variable scripts are under the conda_files directory.

Thanks,  
Matt
