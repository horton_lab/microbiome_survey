#!/bin/sh
export GENERAL_CODE='/Volumes/projects/code/'
export VESCA_MISEQ_DATA='/Volumes/projects/vesca/microbiome/survey/data/'
export VESCA_METADATA='/Volumes/projects/vesca/metadata/'
export VESCA_RESOURCES='/Volumes/projects/vesca/resources/'
export VESCA_OUTPUT_DIR='/Users/<your_own_output_directory>/work/survey/results/'
